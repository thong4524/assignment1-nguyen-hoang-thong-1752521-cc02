
package assifment1_oop;

public class Warrior extends Fighter {
	
	public Warrior(int BaseHp, int Wp){
		super(BaseHp,Wp);
	}
	public double getCombatScore(){
		double combatScore;
		
		if (super.getWp() == 1){
			combatScore = super.getBaseHp();
		}
		else combatScore = super.getBaseHp() / 10;
		
		if (Utility.isPrime(Utility.Ground)){
			combatScore = super.getBaseHp() * 2;
		}
		
		return combatScore;
	}
}
