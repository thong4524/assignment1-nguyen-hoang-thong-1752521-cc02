
package assifment1_oop;

public class Knight extends Fighter {
	
	public Knight(int BaseHp, int Wp){
		super(BaseHp,Wp);
	}
	public double getCombatScore(){
		double combatScore;
		
		if (super.getWp() == 1){
			combatScore = super.getBaseHp();
		}
		else combatScore = super.getBaseHp() / 10;
		
		if (Utility.isSquare(Utility.Ground)){
			combatScore = super.getBaseHp() * 2;
		}
		if (Utility.Ground == 999){
			int t1 = 0, t2 = 1;
		    int nextTerm = t1 + t2;
		
			while(nextTerm <= 2 * super.getBaseHp()){
				t1 = t2;
				t2 = nextTerm;
				nextTerm = t1 + t2;
			}
			combatScore = nextTerm;
		}
		return combatScore;
	}
}
