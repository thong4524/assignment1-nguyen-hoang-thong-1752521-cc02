
package assifment1_oop;

public abstract class Fighter {
	 
	private int   Wp, BaseHp;
	
	public Fighter(int BaseHp, int Wp){
		this.BaseHp = BaseHp;
		this.Wp = Wp;
	}
	 public abstract double getCombatScore();
	 
	 public double getBaseHp(){
		 return BaseHp;
	 }
	 public void setBaseHp(int BaseHp){
		 this.BaseHp = BaseHp;
	 }
	 public int getWp(){
		 return Wp;
	 }
	 public void setWp(int Wp){
		 this.Wp = Wp;
	 }
}
